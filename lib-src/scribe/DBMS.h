#ifndef _SCRIBE_DBMS_H
#define _SCRIBE_DBMS_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#include <map>
#include <string>

namespace scribe {
  /**
   * This class provides access to a database
   * management system, such as MySQL, Postgres, Oracle,
   * or Ingres. <br>
   * In some sense, this class provides the driver for
   * accessing a db.
   */
  class DBMS : public ::timber::Counted {
  private:
    DBMS(const DBMS&);
    DBMS&operator=(const DBMS&);
      
    /** The default constructor */
  protected:
    inline DBMS() throws ()
      {}

    /**
     * Destroy this relational db object. Once all
     * references to this database are gone, all
     * connections will be terminated.
     */
  public:
    virtual ~DBMS() throws();
    
    /**
     * Load a dynamic DBMS from a module.
     * @param modPath the path to a dbms implementation module
     * @throw ::std::exception if the module could not be accessed
     */
  public:
    static ::timber::Reference<DBMS> load( const ::std::string& modPath) throws (::std::exception);
      
    /**
     * Get the name of this DBMS.
     * @return the name of this dbms.
     */
  public:
    virtual ::std::string name() const throws() = 0;
      
    /**
     * Get the name of this DBMS.
     * @return the name of this dbms.
     */
  public:
    virtual ::std::string version() const throws() = 0;    
     
  };
}

#endif
