#include <scribe/DBMS.h>
#include <scribe/DBMSModule.h>
#include <canopy/dso/DSO.h>
#include <timber/logging.h>

#include <stdexcept>

using namespace ::std;
using namespace ::timber::logging;
using ::canopy::dso::DSO;

namespace scribe {
  DBMS::~DBMS() throws()
  {}

    
  ::timber::Reference<DBMS> DBMS::load(const ::std::string& modPath) throws(::std::exception)
  {  
    unique_ptr<DSO> dso;
    try {
      dso = DSO::load(modPath.c_str(),false);
      DBMSModule* mod = static_cast<DBMSModule*>(dso->symbol("dbmsModule"));
      ::timber::Pointer<DBMS> dbms = mod->create();
      if (dbms) {
	Log("scribe.DBMS").info("Loaded DBMS module "+dbms->name()+", version "+dbms->version());
	return dbms;
      }
      dso->close();
      throw ::std::runtime_error("Could not load the DSO properly");
    }
    catch (const ::std::exception& e) {
      Log("scribe.DBMS").caught("Could not load DBMS module from "+modPath,e);
      if (dso.get()) {
	dso->close();
      }
      throw; // rethrow
    }
    catch (...) {
      Log("scribe.DBMS").warn("Could not load DBMS module from "+modPath);
      if (dso.get()) {
	dso->close();
      }
      throw ::std::runtime_error("Module is not a valid DBMSModule");
    }
  }
    
}
