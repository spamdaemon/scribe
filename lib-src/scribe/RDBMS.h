#ifndef _SCRIBE_RDBMS_H
#define _SCRIBE_RDBMS_H

#ifndef _SCRIBE_DBMS_H
#include <scribe/DBMS.h>
#endif

#ifndef _TIMBER_CONFIG_CONFIGURATION_H
#include <timber/config/Configuration.h>
#endif

#ifndef _SCRIBE_SQL_CONNECTION_H
#include <scribe/sql/Connection.h>
#endif

namespace scribe {
  /**
   * This class provides access to a relational
   * database, such as MySQL, Postgres, Oracle, or Ingres.
   */
  class RDBMS : public DBMS {
    CANOPY_BOILERPLATE_PREVENT_COPYING(RDBMS);

    /** Default constructor */
  protected:
    RDBMS () throws();
	
    /** Destructor */
  protected:
    ~RDBMS() throws();

    /**
     * Create an SQL connection to a database. The scheme
     * must be ignored when parsing the URI.
     * @param cfg the connection configuration
     * @return a new conncetion to a database (never 0)
     * @throw exception if the database cannot be accessed
     */
  public:
    virtual ::timber::Reference< ::scribe::sql::Connection> connect (::timber::SharedRef< ::timber::config::Configuration> cfg) const throws (::std::exception) = 0;

  };
}
#endif
