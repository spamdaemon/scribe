#ifndef _SCRIBE_SQL_TRANSACTIONSUPPORT_H
#define _SCRIBE_SQL_TRANSACTIONSUPPORT_H

namespace scribe {
  namespace sql {

    /**
     * Types of transaction that are supported
     * by connections
     */
    enum TransactionSupport { 
      NONE, ///< No transactions are supported
      NESTED, ///< Nested transactions are supported
      BASIC  ///< Basic non-nesting transactions are supported
    };

    /**
     * A transaction isolation level. These are 
     * essentially SQL isolation levels. A database
     * may not support all isolation levels
     */
    enum IsolationLevel {
      READ_UNCOMMITTED, ///< lowest level of isolation (read uncommited data)
      READ_COMMITTED,   ///< read only committed data
      REPEATABLE_READ,  ///< read only committed data, but ensure repeatable results
      SERIALIZABLE      ///< same result result as if statements are executed sequentially
    };

    /**
     * The possible data access levels during a transaction.
     */
    enum AccessLevel {
      READ_ONLY, ///< transaction cannot modify data
      READ_WRITE ///< transaction can read and modify data
    };
  }
}

#endif
