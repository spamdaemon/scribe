#include <scribe/sql/SQLException.h>

namespace scribe {
  namespace sql {
    
    SQLException::SQLException() throws()
      : ::std::runtime_error("Database error") 
    {}

    SQLException::SQLException(const ::std::string& errmsg) throws()
      : ::std::runtime_error(errmsg) 
    {}

    SQLException::SQLException(const char* errmsg) throws()
      : ::std::runtime_error(errmsg) 
    {}
  }
}
