#ifndef _SCRIBE_SQL_SQLSTATEMENT_H
#define _SCRIBE_SQL_SQLSTATEMENT_H

#ifndef _SCRIBE_SQL_STATEMENT_H
#include <scribe/sql/Statement.h>
#endif

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _SCRIBE_SQL_RESULT_H
#include <scribe/sql/Result.h>
#endif

#include <string>
#include <iosfwd>


namespace scribe {
  namespace sql {
    /**
     * This class represents a single statement.
     */
    class SQLStatement : public Statement {
      
      /** The default constructor */
    protected:
      inline SQLStatement () throws()
	{}
      
      /** 
       * Destroy this statement. 
       */
    public:
      ~SQLStatement () throws();
      
      /**
       * Set the statement string. This method will override any string currently
       * set.
       * @param stmt string
       */
    public:
      virtual void setStatement(const ::std::string& stmt) throws () = 0;

      /**
       * Get a stream to which the SQL statement can be written. Any data
       * written to the stream will be appended to any statement current set.
       */
    public:
      virtual ::std::ostream& stream() throws () = 0;

      /**
       * Escape a binary buffer as a string such that it may be used
       * when creating a query. The string will be cleared before appending
       * to it.
       * @param buf a binary buffer
       * @param n the data for the buffer
       * @param str a string into which the escaped buffer is written
       * @throws ::std::exception if an error occurred
       */
    public:
      virtual void escapeBlob (const char* buf, ::canopy::UInt64 n, ::std::string& str) const = 0;
      
    };
  }
}

#endif
