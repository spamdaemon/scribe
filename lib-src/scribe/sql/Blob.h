#ifndef _SCRIBE_SQL_BLOB_H
#define _SCRIBE_SQL_BLOB_H

#ifndef _SCRIBE_SQL_SQLEXCEPTION_H
#include <scribe/sql/SQLException.h>
#endif

#ifndef _TIMBER_COUNTED_H
#include <timber/Counted.h>
#endif

namespace scribe {
  namespace sql {

    /**
     * This is the base class for all SQL values.
     * SQLValues are immutable and subclasses should respect that.
     */
    class Blob : public ::timber::Counted {
	  
      /**
       * Default constructor.
       */
    protected:
      inline Blob () throws()
	{}

      /** Destroy this type */
    public:
      virtual ~Blob() throws() = 0;
      
      /**
       * Get the size of this blob object.
       * @return the number of bytes in this blob
       */
    public:
      virtual ::canopy::UInt64 size() const throws () = 0;

      /**
       * Get a byte in this blob.
       * @param i a byte index
       * @return the byte at the specified index
       * @throws SQLException if the byte could not be retrieved
       */
    public:
      virtual char at(::canopy::UInt64 i) const throws(SQLException) = 0;

      /**
       * Get the specified byte
       * @param i a byte index
       * @return the byte at the specifeid index
       * @throws SQLException if the byte could not be retrieved
       */
     public:
      inline char operator[] (::canopy::UInt64 i) const throws (SQLException)
        { return at(i); }

    };
  }
}

#endif
