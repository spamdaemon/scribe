#ifndef _SCRIBE_SQL_CONNECTION_H
#define _SCRIBE_SQL_CONNECTION_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _SCRIBE_SQL_SQLEXCEPTION_H
#include <scribe/sql/SQLException.h>
#endif

#ifndef _SCRIBE_SQL_SQLSTATEMENT_H
#include <scribe/sql/SQLStatement.h>
#endif

#ifndef _SCRIBE_SQL_SCHEMA_H
#include <scribe/sql/Schema.h>
#endif

#ifndef _SCRIBE_SQL_TRANSACTIONSUPPORT_H
#include <scribe/sql/TransactionSupport.h>
#endif

#ifndef _SCRIBE_SQL_SQLTYPE_H
#include <scribe/sql/SQLType.h>
#endif

namespace scribe {
  namespace sql {

    /**
     * This class represents a single connection to a 
     * database. All communication with the database
     * will be performed via this interface.
     */
    class Connection : public ::timber::Counted 
    {
      /** The default constructor */
    protected:
      inline Connection () throws()
	{}
      
      /** 
       * Destroy this connection. 
       */
    public:
      ~Connection () throws();
      
      /**
       * Start a transaction. If no transactions or nested transactions
       * are supported, then this method must return false.
       * @note If false is returned, then the corresponding abortTransaction() or completeTransaction()
       * statements will have no real effect.
       * @param il an isolatation level
       * @param al an access level
       * @return true if no transaction was in progress or nested transactions are supported
       * @throw SQLException if the transaction could not be started or th isolation level is not supported
       */
    public:
      bool startTransaction (IsolationLevel il, AccessLevel al) throws(SQLException);

      /**
       * Start a READ_WRITE transaction with the specified isolation level.
       * @param il an isolatation level
       * @return true if no transaction was in progress or nested transactions are supported
       * @throw SQLException if the transaction could not be started or th isolation level is not supported
       */
    public:
      inline bool startTransaction (IsolationLevel il) throws(SQLException)
	{ return startTransaction(il,READ_WRITE); }

      /**
       * Start a SERIALIZABLE and READ_WRITE transaction. 
       * @return true if no transaction was in progress or nested transactions are supported
       * @throw SQLException if the transaction could not be started or are not supported
       */
    public:
      inline bool startTransaction () throws(SQLException)
	{ return startTransaction(SERIALIZABLE); }

      /**
       * Finish the transaction. This commits any changes that have been made during
       * the transaction.
       * @throw SQLException if some condition prevente the transaction from being committed
       */
    public:
      void finishTransaction() throws(SQLException);

      /**
       * Abort the current transaction. Any changes made since the last
       * startTransaction() invokation will be undone.
       * @throw SQLException if some condition prevente the transaction from being aborted
       */
    public:
      void abortTransaction () throws(SQLException);

      /**
       * Create an SQL statement.
       * @return a new statement
       */
    public:
      virtual ::timber::Reference<SQLStatement> createSQLStatement() throws (SQLException) = 0;
      
      /**
       * Set all constraints to be evaluated at completion of the current transaction.
       * @param deferred if true, then delay constraint evaluation until end of transaction
       * @throw SQLException if the constraints cannot be changed
       */
    public:
      virtual void setDeferredConstraintsEnabled (bool deferred) throws(SQLException) = 0;

      /**
       * Get the schema. Throws the AccessException by default.
       * @return the schema
       * @throw SQLException if the schema could not be loaded
       */
    public:
      //virtual ::timber::Reference<Schema> schema() throws(SQLException) = 0;

      /**
       * Test if there is an open transaction.
       * @return true if a transaction is currently in progress
       */
    public:
      virtual bool isTransactionOpen() const throws() = 0;
      
      /**
       * Get the type of transaction that is supported.
       * @return NONE by default
       */
    public:
      virtual TransactionSupport getTransactionSupport() const throws() = 0;
      
      /**
       * Check if this connection supports the specified isolation level.
       * @param level an isolation level
       * @return true if this connection supports the specified transaction isolation level
       */
    public:
      virtual bool supportsIsolationLevel (IsolationLevel level) const throws() = 0;

      /**
       * Check if this connection supports access levels
       * @return true if this connection supports use of access levels for transactions.
       */
    public:
      virtual bool supportsAccessLevels () const throws() = 0;

      /**
       * Start a transaction. If no transactions or nested transactions
       * are supported, then this method must return false.
       * @note If false is returned, then the corresponding abortTransaction() or completeTransaction()
       * statements will have no real effect.
       * @param il an isolatation level
       * @param al an access level
       * @return true if no transaction was in progress or nested transactions are supported
       * @throw SQLException if the transaction could not be started or th isolation level is not supported
       */
    protected:
      virtual bool beginTransaction (IsolationLevel il, AccessLevel al) throws(SQLException) = 0;

      /**
       * Commit the transaction
       */
    protected:
      virtual void commitTransaction() throws(SQLException) = 0;

      /**
       * Rollback the transaction.
       */
    protected:
      virtual void rollbackTransaction() throws(SQLException) = 0;

      /**
       * Get an sql type object.
       * @param id the type id
       * @return the corresponding type
       * @throws SQLException if the type is not supported
       */
    public:
      virtual ::timber::Reference<SQLType> createType (SQLType::ID id) const throws(SQLException) = 0;
    };
  }
}

#endif
