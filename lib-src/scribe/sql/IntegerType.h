#ifndef _SCRIBE_SQL_SQLINTEGERTYPE_H
#define _SCRIBE_SQL_SQLINTEGERTYPE_H

#ifndef _SCRIBE_SQL_SQLTYPE_H
#include <scribe/sql/SQLType.h>
#endif

namespace scribe {
  namespace sql {

    /**
     * This class represents SQL character types CHAR, VARCHAR.
     */
    class IntegerType : public SQLType {
      IntegerType(const IntegerType&);
      IntegerType& operator=(IntegerType&);

      /**
       * Create a new char type. 
       */
    protected:
      inline IntegerType () throws() {}

	
      /** Destroy this type */
    protected:
      ~IntegerType() throws();
	
      /**
       * Get the number of bytes used to represent this type in the database.
       * @return the number of bytes used to represent this type in the database
       */
    public:
      virtual ::canopy::UInt32 byteCount() const throws() = 0;
	
      /**
       * Get the type id
       * @return the id for this integer type.
       */
    public:
      virtual ID id() const throws() = 0;
    };
  }
}

#endif
