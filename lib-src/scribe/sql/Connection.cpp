#include <scribe/sql/Connection.h>
#include <timber/logging.h>

using namespace ::timber::logging;

namespace scribe {
  namespace sql {
    
    Connection::~Connection() throws()
    {}

    bool Connection::startTransaction (IsolationLevel il, AccessLevel al) throws(SQLException)
    { 
      Log("scribe::sql::Connection::startTransaction").info("Begin transaction");
      try {
	return beginTransaction(il,al); 
      }
      catch (const SQLException& e) {
	LogEntry("scribe::sql::Connection::startTransaction",Level::EXCEPTION).stream() 
	  << "Could not start transaction : " << e.what()
	  << commit;
	throw;
      }
    }
    
    void Connection::finishTransaction() throws(SQLException)
    {
      commitTransaction(); 
      try {
	Log("scribe::sql::Connection::finishTransaction").info("Transaction complete");
      }
      catch (const SQLException& e) {
	LogEntry("scribe::sql::Connection::finishTransaction",Level::EXCEPTION).stream() 
	  << "Could not complete transaction : " << e.what()
	  << commit;
	throw;
      }
    }
    
    void Connection::abortTransaction () throws(SQLException)
    { 
      Log("scribe::sql::Connection::abortTransaction").info("Transaction aborted");
      try {
	rollbackTransaction(); 
      }
      catch (const SQLException& e) {
	LogEntry("scribe::sql::Connection::abortTransaction",Level::EXCEPTION).stream() 
	  << "Could not abort transaction : " << e.what()
	  << commit;
	throw;
      }
    }


  }
}
