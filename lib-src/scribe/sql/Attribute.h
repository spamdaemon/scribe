#ifndef _SCRIBE_SQL_ATTRIBUTE_H
#define _SCRIBE_SQL_ATTRIBUTE_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _SCRIBE_SQL_SQLTYPE_H
#include <scribe/sql/SQLType.h>
#endif

namespace scribe {
  namespace sql {

    /**
     * This class is used to represent a column in a Table.
     */
    class Attribute : public ::timber::Counted {
      Attribute(const Attribute&);
      Attribute&operator=(const Attribute&);

      /**
       * Create a new attribute.
       */
    protected:
      Attribute () throws();
	  
      /** Destroy this attribute */
    public:
      ~Attribute() throws();

      /**
       * Get the  name of this attribute.
       * @return the lowercase string form of the attribute
       */
    public:
      virtual ::std::string name() const throws() = 0;

      /**
       * Get the attribute type.
       * @return the type for this attribute
       */
    public:
      virtual ::timber::Reference<SQLType> type() const throws() = 0;

      /**
       * Test if this is a required or optional attribute.
       * @return true if this attribute can be null
       */
    public:
      virtual bool isRequired() const throws() = 0;

    };
  }
}

#endif
