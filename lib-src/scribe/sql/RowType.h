#ifndef _SCRIBE_SQL_ROWTYPE_H
#define _SCRIBE_SQL_ROWTYPE_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _SCRIBE_SQL_SQLEXCEPTION_H
#include <scribe/sql/SQLException.h>
#endif

#ifndef _SCRIBE_SQ_SQLTYPE_H
#include <scribe/sql/SQLType.h>
#endif

namespace scribe {
  namespace sql {
      
    /** Forward declare a row */
    class Row;

    /**
     * This class defines the type for a row.
     */
    class RowType : public ::timber::Counted {
      RowType(const RowType&);
      RowType&operator=(const RowType&);

      /** A column index */
    public:
      typedef ::canopy::UInt32 ColumnIndex;

      /**
       * Create a row type
       */
    protected:
      inline RowType () throws()
	{}

	/** Destroy a row reference */
    public:
      ~RowType () throws();

      /**
       * Get the number of columns for rows of this type.
       * @return the number of columns for rows of this type.
       */
    public:
      virtual ColumnIndex columnCount () const throws() = 0;

      /**
       * Get the index associated with the specified column name.
       * @param name a column name
       * @return the index of the column corresponding to the name or invalidIndex().
       * @throw SQLException if the index cannot be determined
       */
    public:
      virtual ColumnIndex columnIndex (const ::std::string& name) const throws(SQLException) = 0;
	
      /**
       * Get the name of the column at the specified index.
       * @param col a column index
       * @return the name of the specified column, or 0 if there is no such column
       * @throw SQLException if the name cannot be determined
       */
    public:
      virtual ::std::string columnName (ColumnIndex col) const throws(SQLException) = 0;

      /**
       * Get the sql type for the specified column.
       * @return the sql type for the specified column.
       * @throw SQLException if the type cannot be determined
       */
    public:
      virtual ::timber::Reference<SQLType> sqlType (ColumnIndex col) const throws(SQLException) = 0;
    };
  }
}

#endif
