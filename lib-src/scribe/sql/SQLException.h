#ifndef _SCRIBE_SQL_SQLException_H
#define _SCRIBE_SQL_SQLException_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#include <stdexcept>
#include <string>

namespace scribe {
  namespace sql {
    /**
     * This exception class is the base class for
     * all exceptions caused by a DBMS operation.
     */
    class SQLException : public ::std::runtime_error {

      /**
       * Create a new Database exception.
       */
    public:
      SQLException() throws();
      
      /**
       * Create a new Database exception.
       * @param reason the reason
       */
    public:
      SQLException(const ::std::string& reason) throws();
      
      /**
       * Create a new Database exception.
       * @param reason the reason
       */
    public:
      SQLException(const char* reason) throws();

      /** Destructor */
    public:
      ~SQLException() throw() {}

    };
  }
}
#endif
