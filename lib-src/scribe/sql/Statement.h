#ifndef _SCRIBE_SQL_STATEMENT_H
#define _SCRIBE_SQL_STATEMENT_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _SCRIBE_SQL_RESULT_H
#include <scribe/sql/Result.h>
#endif

#ifndef _SCRIBE_SQL_SQLEXCEPTION_H
#include <scribe/sql/SQLException.h>
#endif

#include <map>
#include <string>

namespace scribe {
  namespace sql {
    /**
     * This class is the base class for all types of statements.
     */
    class Statement : public ::timber::Counted {
      /**
       * Statement parameters
       */
    public:
      typedef ::std::map< ::std::string, ::std::string> Parameters;
      
      /** The default constructor */
    protected:
      inline Statement () throws()
	{}
      
      /** 
       * Destroy this statement. 
       */
    public:
      ~Statement () throws();
      
      /**
       * Set parameters for this statement. This replaces any existing
       * parameters with a new set. To remove existing parameters, simply
       * use an empty map. The parameters will remain in effect for the duration
       * of this statement or until they are cleared.
       * @param parms a set of parameters
       */
    public:
      virtual void setParameters (const Parameters& parms) throws(SQLException) = 0;
      
       /**
       * Set a single parameter. This replaces any existing
       * parameter with the same name.
       * The parameters will remain in effect for the duration
       * of this statement or until they are cleared.
       * @param name the parameter name
       * @param value the parameter value
       */
    public:
      virtual void setParameter (const ::std::string& name, const ::std::string& value) throws(SQLException) = 0;
      
      /**
       * Clear the specified parameter
       * @param name the parameter to be unset
       */
    public:
      virtual void clearParameter(const ::std::string& name) throws(SQLException) = 0;

      /**
       * Execute a statement.
       * @return a result or 0 if this was not a query
       * @throw SQLException if this statement could not be executed or some error occurred
       */
    public:
      virtual ::timber::Reference<Result> execute () const throws(SQLException) = 0;
    };
  }
}

#endif
