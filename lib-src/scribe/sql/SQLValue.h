#ifndef _SCRIBE_SQL_SQLVALUE_H
#define _SCRIBE_SQL_SQLVALUE_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#include <memory>

namespace scribe {
  namespace sql {

    /**
     * This is the base class for all SQL values.
     * SQLValues are immutable and subclasses should respect that.
     */
    class SQLValue {
	  
      /**
       * Default constructor.
       */
    protected:
      inline SQLValue () throws()
	{}

      /** Destroy this type */
    protected:
      virtual ~SQLValue() throws() = 0;

      /**
       * Create a copy of this value
       * @return a copy
       * @throws ::std::exception if the value could not be cloned
       */
    public:
      virtual ::std::unique_ptr<SQLValue> clone() const throws (::std::exception) = 0;

      /**
       * Compare two values. If the two values are 
       * not comparable, then false will be returned
       * @param v a value
       * @return return type().equals(v.type()) && value()==v.value()
       */
    public:
      virtual bool equals (const SQLValue& v) const throws() = 0;
	
      /**
       * Get the value as a string. The string may be 0 to indicate a null value.
       * @return the string representing this value or 0 if this is a null-value.
       */
    public:
      virtual const char* stringValue() const throws() = 0;
	
      /**
       * Test if this is a null value. The default
       * is <code> return stringValue()==0</code>.
       * @return true if this value represents null
       */
    public:
      virtual bool isNull () const throws() = 0;
    };
  }
}

#endif
