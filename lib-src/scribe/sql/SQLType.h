#ifndef _SCRIBE_SQL_SQLTYPE_H
#define _SCRIBE_SQL_SQLTYPE_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#include <string>

namespace scribe {
  namespace sql {
    
    /** A SQL value */
    class SQLValue;

    /**
     * This class provides a way to reference SQL types.
     */
    class SQLType : public ::timber::Counted {
	
      /**
       * The various SQL type ids.
       */
    public:
      enum ID { 
	EXTENSION,
	INTEGER,
	SMALLINT,
	CHAR,
	VARCHAR,
	BOOLEAN,
	NUMERIC,
	DECIMAL,
	REAL,
	DOUBLE,
	FLOAT,
	CLOB,
	DATE,
	TIME,
	BIT,
	VARBIT,
	BLOB,
	INTERVAL
      };

      /**
       * Create a new SQL type with the specified name.
       * The subclass must implement the getTypeName() method!
       */
    protected:
      inline SQLType () throws()
	{}
	
      /** Destroy this type */
    protected:
      ~SQLType() throws();

      /**
       * Test if the specified type is an SQL varying type.
       * @param t a type
       * @return true if t==VARCHAR || t==VARBIT
       */
    public:
      inline static bool isVaryingType (ID t) throws()
	{ return t==VARCHAR || t==VARBIT; }
      
      /**
       * Test if the specified type is an SQL bounded type.
       * @param t a type
       * @return true if t==VARCHAR || t==VARBIT
       */
    public:
      inline static bool isBoundedType (ID t) throws()
	{ return isVaryingType(t) || t==CHAR || t==BIT; }
      
      /**
       * Test if this is a bounded type. Returns
       * false by default.
       * @return true if this type requires a length argument.
       */
    public:
      virtual bool isBounded() const throws() = 0;

      /**
       * Test if this is a varying type. A varying type
       * is for example, VARBIT, or VARCHAR. Returns
       * false by default.
       * @return true if this type is a varying type
       */
    public:
      virtual bool isVarying () const throws() = 0;

      /**
       * Compare two types. 
       * @param v a value
       * @return true if name()==v.name()
       */
    public:
      virtual bool equals (const SQLType& v) const throws() = 0;
	
      /**
       * Get the SQL type id for this type.
       * @return the id for this type
       */
    public:
      virtual ID id() const throws() = 0;

      /**
       * Get the SQL name for this type. This method
       * uses id() to generate type strings for all SQL types,
       * but not for EXTENSION types.
       * @return the string form of the type
       */
    public:
      virtual ::std::string name() const throws() = 0;

      /**
       * Create a value string for this type.
       * If this is a character type, then s
       * will be surrounded by single quotes.
       * @param v a value
       * @return a properly escaped string.
       */
    public:
      virtual const ::std::string toStringValue (const SQLValue& v) const throws() = 0;
    };
  }
}
#endif
