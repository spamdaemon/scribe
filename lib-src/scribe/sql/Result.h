#ifndef _SCRIBE_SQL_RESULT_H
#define _SCRIBE_SQL_RESULT_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace scribe {
  namespace sql {
    /**
     * This class represents the result from a statement.
     */
    class Result : public ::timber::Counted {
      
      /** The default constructor */
    protected:
      inline Result () throws()	{}
     
      /** 
       * Destroy this result. 
       */
    public:
      ~Result () throws();

      /** 
       * Get the number of rows that were affected by 
       * the command.
       * @return the number of query results or -1 if there was no query
       */
    public:
      virtual ::canopy::UInt64 rowCount () const throws() = 0;
    };
  }
}

#endif
