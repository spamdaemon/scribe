#ifndef _SCRIBE_SQL_SCHEMA_H
#define _SCRIBE_SQL_SCHEMA_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _SCRIBE_SQL_SQLEXCEPTION_H
#include <scribe/sql/SQLException.h>
#endif

#ifndef _SCRIBE_SQL_TABLE_H
#include <scribe/sql/Table.h>
#endif

#include <string>

namespace scribe {
  namespace sql {

    /**
     * This class is used to represent a column in a Schema.
     */
    class Schema : public ::timber::Counted {
      Schema(const Schema&);
      Schema&operator=(const Schema&);
	
      /**
       * Create a new schema.
       */
    protected:
      inline Schema () throws()
	{}

      /** Destroy this schema */
    protected:
      ~Schema() throws();

      /** 
       * Get the name for this schema.
       * Returns 0 by default.
       * @return the schema name or 0 
       */
    public:
      virtual ::std::string name() const throws() = 0;


      /**
       * Get the number of tables in this schema.
       * @return the number of tables.
       */
    public:
      virtual ::canopy::UInt32 tableCount() const throws() = 0;

      /**
       * Get the specified table.
       * @param i an table index
       * @return the specified table
       */
    public:
      virtual ::timber::Reference<Table> table(::canopy::UInt32 i) const throws(SQLException) = 0;

      /**
       * Get the specified table by its name.
       * @param n a table name
       * @return the specified table or 0 if it doesn't exist
       */
    public:
      virtual  ::timber::Reference<Table> tableByName(const ::std::string& n) const throws(SQLException) = 0;
    };
  }
}

#endif
