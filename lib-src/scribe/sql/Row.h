#ifndef _SCRIBE_SQL_ROW_H
#define _SCRIBE_SQL_ROW_H

#ifndef _TIMBER_DATE_H
#include <timber/Date.h>
#endif

#ifndef _TIMBER_TIMEOFDAY_H
#include <timber/TimeOfDay.h>
#endif

#ifndef _SCRIBE_SQL_SQLEXCEPTION_H
#include <scribe/sql/SQLException.h>
#endif

#ifndef _SCRIBE_SQL_BLOB_H
#include <scribe/sql/Blob.h>
#endif

#include <string>

namespace scribe {
  namespace sql {

    /**
     * This class represents a single result to a 
     * database. All communication with the database
     * will be performed via this interface.
     */
    class Row {
      Row(const Row&);
      Row&operator=(const Row&);

      /** A column index */
    public:
      typedef ::canopy::UInt32 ColumnIndex;

      /** Default constructor */
    protected:
      inline Row () throws()
	{}
	
      /** Destructor */
    protected:
      virtual ~Row () throws();
      
      /** 
       * Get the number of columns in the result set
       * @return the number of columns in the result or -1 if there was no query
       */
    public:
      virtual ::canopy::UInt32 columnCount () const throws() = 0;

      /**
       * Test if the value in the specified row is 0.
       * @param col a column index
       * @pre REQUIRE_RANGE(col,0,columnCount()-1)
       * @return true if the value in the specified row is 0
       */
    public:
      virtual bool isNull (ColumnIndex col) const throws(SQLException) = 0;

      /**
       * Get the string that represents the value in the specified column. If the
       * the column is a null-value, then this method return an empty string.
       * @param col a column index
       * @pre REQUIRE_RANGE(i,0,columnCount()-1)
       * @return the string representation of the value in the specified column
       */
    public:
      virtual ::std::string stringValue (ColumnIndex col) const throws(SQLException) = 0;
      
      /**
       * Get the value in the specified column as an Long
       * @param col a column index
       * @return the integer value in the specified column
       * @throw Exception if the column value cannot be converted to an long
       * @throw Exception if the <code>isNull(col)</code>
       */
    public:
      virtual ::canopy::Int64 longValue(ColumnIndex col) const throws(SQLException)= 0;
	
      /**
       * Get the value in the specified column as an Int
       * @param col a column index
       * @return the integer value in the specified column
       * @throw Exception if the column value cannot be converted to an int
       * @throw Exception if the <code>isNull(col)</code>
       */
    public:
      virtual ::canopy::Int32 intValue(ColumnIndex col) const throws(SQLException)= 0;

      /**
       * Get the value in the specified column as an Double
       * @param col a column index
       * @return double value in the specified column
       * @throw Exception if the column value cannot be converted to an double
       * @throw Exception if the <code>isNull(col)</code>
       */
    public:
      virtual ::canopy::Double doubleValue(ColumnIndex col) const throws(SQLException)= 0;

      /**
       * Get the value in the specified column as an Float
       * @param col a column index
       * @return double value in the specified column
       * @throw Exception if the column value cannot be converted to an double
       * @throw Exception if the <code>isNull(col)</code>
       */
    public:
      virtual ::canopy::Float floatValue(ColumnIndex col) const throws(SQLException)= 0;

      /**
       * Get the value in the specified column as a boolean.
       * @param col a column index
       * @return the boolean value
       * @throw Exception if the column value cannot be converted to an bool
       * @throw Exception if the <code>isNull(col)</code>
       */
    public:
      virtual bool boolValue(ColumnIndex col) const throws(SQLException)= 0;

      /**
       * Get the value in the specified column as a date.
       * @param col a column index
       * @return the date value
       * @throw Exception if the column value cannot be converted to an date
       * @throw Exception if the <code>isNull(col)</code>
       */
    public:
      virtual ::timber::Date dateValue(ColumnIndex col) const throws(SQLException)= 0;

      /**
       * Get the value in the specified column as a time.
       * @param col a column index
       * @return the date value
       * @throw Exception if the column value cannot be converted to a time
       * @throw Exception if the <code>isNull(col)</code>
       */
    public:
      virtual ::timber::TimeOfDay timeValue(ColumnIndex col) const throws(SQLException)= 0;

      /**
       * Get a pointer to a blob.
       * @param col a column index
       * @return a pointer to a blob or 0 if the blob is null
       * @throws SQLException the blob cannot be retrieved
       */
    public:
      virtual ::timber::Pointer<Blob> blobValue(ColumnIndex col) const throws(SQLException)=0;

      /*@}*/
    };
  }
}

#endif
