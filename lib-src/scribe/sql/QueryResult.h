#ifndef _SCRIBE_SQL_SQLQUERYRESULT_H
#define _SCRIBE_SQL_SQLQUERYRESULT_H

#ifndef _SCRIBE_SQL_SQLEXCEPTION_H
#include <scribe/sql/SQLException.h>
#endif

#ifndef _SCRIBE_SQL_RESULT_H
#include <scribe/sql/Result.h>
#endif

#ifndef _SCRIBE_SQL_ROWTYPE_H
#include <scribe/sql/RowType.h>
#endif

namespace scribe {
  namespace sql {
    
    /** A row */
    class Row;

    /**
     * This class represents a single result to a 
     * database. All communication with the database
     * will be performed via this interface.
     */
    class QueryResult : public Result {
      QueryResult(const QueryResult&);
      QueryResult&operator=(const QueryResult&);

      /** Default constructor */
    protected:
      inline QueryResult () throws()
	{}
	
      /** Destructor */
    protected:
      ~QueryResult () throws();

      /** 
       * Get the number of columns in the result set
       * @return the number of columns in the result or -1 if there was no query
       */
    public:
      virtual ::canopy::UInt32 columnCount () const throws() = 0;

      /**
       * Get information about the rows in this result.
       * @return information about the rows in this result
       * @throw SQLException if the information cannot be obtained
       */
    public:
      virtual ::timber::Reference<RowType> info() const throws(SQLException) = 0;

      /**
       * Get the index associated with the specified column name. This information may also
       * be retrieved via the info(), but it might be faster to quickly retrieve the index
       * of a column.
       * @param name a column name
       * @return the index of the column corresponding to the name or invalidIndex().
       * @throw SQLException if the index cannot be determined
       */
    public:
      virtual ::canopy::UInt32 columnIndex (const ::std::string& name) const throws(SQLException) = 0;
	
      /**
       * Get the name of the column at the specified index.
       * @param col a column index
       * @return the name of the specified column, or 0 if there is no such column
       * @throw SQLException if the name cannot be determined
       */
    public:
      virtual ::std::string columnName (::canopy::UInt32 col) const throws(SQLException) = 0;

      /**
       * Access the current row.
       * @return a pointer to the current row
       * @throws SQLException if the cursor is not positioned on a row.
       */
    public:
      virtual Row* operator->() throws (SQLException) = 0;

      /**
       * Scroll by one. This is equivalent to scroll(1)
       * @return true if the scroll(1) would return true, false if it would scroll beyond the last row
       * @throws SQLException if a database error occured while scrolling
       */
    public:
      virtual bool scroll() throws (SQLException) = 0;

      /**
       * Scroll by the specified number of rows. A negative number scrolls
       * towards the beginning of the result, and a positive number scrolls
       * to the end of the result.
       * @param delta the number of rows to scroll 
       * @throws SQLException if a database error occured while scrolling
       */
    public:
      virtual bool scroll (::canopy::Int64 delta) throws (SQLException) = 0;

      /**
       * Scroll to the specified row.
       * @param r the row to which to scroll
       * @return true if current row indicator was positioned on the specified row, false otherwise
       */
    public:
      virtual bool scrollTo(::canopy::UInt64 r) throws (SQLException) = 0;

      /**
       * Get the current cursor position. 
       * @return the current cursor position
       * @throws SQLException if rowCount()==0
       */
    public:
      virtual ::canopy::UInt64 row() const throws (SQLException) = 0;
      
    };
  }
}

#endif
