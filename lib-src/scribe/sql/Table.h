#ifndef _SCRIBE_SQL_TABLE_H
#define _SCRIBE_SQL_TABLE_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _SCRIBE_SQL_ATTRIBUTE_H
#include <scribe/sql/Attribute.h>
#endif

#ifndef _SCRIBE_SQL_CONSTRAINT_H
#include <scribe/sql/Constraint.h>
#endif

namespace scribe {
  namespace sql {

    /**
     * This class represents a single table in schema.
     */
    class Table : public ::timber::Counted {
      Table(const Table&);
      Table&operator=(const Table&);

      /**
       * Create a new table. 
       */
    protected:
      inline Table () throws()
	{}

      /** Destroy this table */
    protected:
      ~Table() throws();

      /** 
       * Get the name for this table.
       * @return the table name in lower case.
       */
    public:
      virtual ::std::string name() const throws() = 0;
	
      /**
       * Get the number of attributes in this table.
       * @return the number of attributes.
       */
    public:
      virtual ::canopy::UInt32 attributeCount() const throws() = 0;

      /**
       * Get the specified attribute.
       * @param i an attribute index
       * @return the specified attribute
       */
    public:
      virtual ::timber::Reference<Attribute> attribute(::canopy::UInt32 i) const throws(SQLException) = 0;

      /**
       * Get the number of constraints in this table.
       * @return the number of constraints.
       */
    public:
      virtual ::canopy::UInt32 constraintCount() const throws() = 0;

      /**
       * Get the specified constraint.
       * @param i an constraint index
       * @return the specified constraint
       */
    public:
      virtual ::timber::Reference<Constraint> constraint(::canopy::UInt32 i) const throws(SQLException) = 0;

      /**
       * Get the attribute that is the primary key. Returns 0 by default.
       * @return the attribute which is the primary key or 0 if there isn't one
       */
    public:
      virtual ::timber::Pointer<Constraint> primaryKey () const throws() = 0;
    };
  }
}

#endif
