#ifndef _SCRIBE_SQL_CONSTRAINT_H
#define _SCRIBE_SQL_CONSTRAINT_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _SCRIBE_SQL_ATTRIBUTE_H
#include <scribe/sql/Attribute.h>
#endif

#ifndef _SCRIBE_SQL_SQLEXCEPTION_H
#include <scribe/sql/SQLException.h>
#endif

namespace scribe {
  namespace sql {

    /**
     * This class represents a table constraint.
     */
    class Constraint : public  ::timber::Counted {
      Constraint(const Constraint&);
      Constraint&operator=(const Constraint&);
      
      /** The various kinds of constraints. */
    public:
      enum Type { UNIQUE, PRIMARY_KEY, FOREIGN_KEY, OTHER };
      
      /** Create a new constraint. */
    protected:
      inline Constraint () throws()
	{}
      
      /** Destroy this constraint */
    protected:
      ~Constraint() throws();
      
      /**
       * Get the  name of this constraint.
       * @return the lowercase string form of the constraint or an empty string if this constraint has no name
       */
    public:
      virtual ::std::string name() const throws() = 0;

      /**
       * Create a string representation of this constraint.
       * @return a string representation of this constraint
       */
    public:
      virtual ::std::string toString() const throws() = 0;

      /**
       * Get the number of attributes that are part of this constraint.
       * @return the number of attributes that make up this constraint
       */
    public:
      virtual ::canopy::UInt32	attributeCount() const throws() = 0;

      /**
       * Get the number of attributes that are part of this constraint.
       * @param i an attribute index
       * @pre REQUIRE_RANGE(i,0,attributeCount()-1)
       * @return the number of attributes that make up this constraint
       */
    public:
      virtual ::timber::Reference<Attribute> attribute(::canopy::UInt32 i) const throws(SQLException) = 0;

      /**
       * Get the type of constraint.
       * @return the type of constraint.
       */
    public:
      virtual Type type () const throws() = 0;

      /**
       * If this is a foreign key constraint, then this method returns the 
       * name of the referenced table.
       * @return the table referenced by a foreign key or an empty string if this is not a foreign key constraint
       */
    public:
      virtual ::std::string foreignKeyTable() const throws() = 0;
	
      /**
       * Get the attribute in the foreign key table 
       * @param i an attribute in the foreign key table.
       * @pre REQUIRE_RANGE(i,0,attributeCount())
       * @return an attribute
       */
    public:
      virtual ::timber::Reference<Attribute> foreignKeyAttribute(::canopy::UInt32 i) const throws() = 0;
    };
  }
}

#endif
