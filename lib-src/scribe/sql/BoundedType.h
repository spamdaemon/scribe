#ifndef _SCRIBE_SQL_BOUNDEDTYPE_H
#define _SCRIBE_SQL_BOUNDEDTYPE_H

#ifndef _SCRIBE_SQL_SQLTYPE_H
#include <scribe/sql/SQLType.h>
#endif

namespace scribe {
  namespace sql {
    /**
     * This class represents bounded SQL types such as CHAR, VARCHAR, BIT, VARBIT.
     * CLOB is not considered a bounded SQLtype, because it length is unlimited.
     */
    class BoundedType : public SQLType {
      BoundedType(const BoundedType&);
      BoundedType& operator=(BoundedType&);
      
      /**
       * Create a new bounded EXTENSION type.
       */
    protected:
      inline BoundedType () throws() {}
      
      /** Destroy this type */
    protected:
      ~BoundedType() throws();

      /**
       * Get the maximum string length.
       * @return the maximum string length.
       */
    public:
      virtual  ::canopy::UInt32 maxLength() const throws() = 0;
    };
  }
}

#endif
