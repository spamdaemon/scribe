#ifndef _SCRIBE_DBMSMODULE_H
#define _SCRIBE_DBMSMODULE_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _SCRIBE_DBMS_H
#include <scribe/DBMS.h>
#endif

extern "C" {

  struct DBMSModule {

    /** 
     * This function instantiates a new DBMS object. This function must not throw
     * an exception.
     * @return a pointer to a DBMS object, 0 if an error occurred
     */
  public:
    typedef ::timber::Pointer< ::scribe::DBMS> (*CreateDBMS) ();
    
    /** 
     * Create a new DBMS. @see CreateDBMS.
     */
  public:
    CreateDBMS create;
  };
}

#endif
