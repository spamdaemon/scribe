#include <iostream>
#include <scribe/sql.h>

using namespace ::scribe;
using namespace ::scribe::sql;
using namespace ::timber;
using namespace ::canopy;
 
void test1()
{
  Pointer<RDBMS> rdbms;
  Pointer<Table> table;
  Pointer<QueryResult> res;
  Pointer<Statement> stmt;
  Pointer<Connection> con;
}

int main()
{
  try {
    test1();
  }
  catch (const ::std::exception& e) {
    ::std::cerr << "Exception caught " << e.what() << ::std::endl;
    return 1;
  }
  catch (...) {
    ::std::cerr << "Unknown exception caught " << ::std::endl;
    return 1;
  }
  return 0;
}
