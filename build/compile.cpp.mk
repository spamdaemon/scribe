# this fragment defines how to compile source code 
# it defines functions such as compile.cpp, compile.c 
# to compile C++ and C code

define compile.cpp
	g++ -c -fPIC -o $@ $< \
	$(CPP_WARNING_OPTS)\
	$(CPP_STANDARD_OPTS) \
	$(EXT_CPP_OPTS) \
        $(if $(filter true,$(COVERAGE)), $(COMPILER_COVERAGE_OPTS)) \
        $(if $(filter true,$(OPTIMIZE)), $(CPP_OPTIMIZE_OPTS),$(CPP_DEBUG_OPTS)) \
	$(addprefix -I,$(BASE_INC_SEARCH_PATH) $(PROJ_INC_SEARCH_PATH) $(EXT_INC_SEARCH_PATH)) \
	$(addprefix -D,$(CPP_DEFINES) $(PROJ_COMPILER_DEFINES) $(EXT_COMPILER_DEFINES))
endef


define compile.test.cpp
	g++ -c -fPIC -o $@ $< \
	$(CPP_WARNING_OPTS)\
	$(CPP_STANDARD_OPTS) \
	$(EXT_CPP_OPTS) \
        $(CPP_DEBUG_OPTS) \
	$(addprefix -I,$(BASE_INC_SEARCH_PATH) $(PROJ_INC_SEARCH_PATH) $(EXT_INC_SEARCH_PATH)) \
	$(addprefix -D,$(CPP_DEFINES) $(PROJ_COMPILER_DEFINES) $(EXT_COMPILER_DEFINES))
endef

define make.depend.cpp
	g++ -MM -MG -MT $(patsubst %.depend,%.o,$@) -MF $@ $< \
	$(CPP_WARNING_OPTS)\
	$(CPP_STANDARD_OPTS) \
	$(EXT_CPP_OPTS) \
	$(addprefix -I,$(BASE_INC_SEARCH_PATH) $(PROJ_INC_SEARCH_PATH) $(EXT_INC_SEARCH_PATH)) \
	$(addprefix -D,$(CPP_DEFINES) $(PROJ_COMPILER_DEFINES) $(EXT_COMPILER_DEFINES))
endef

define check.cpp
	cppcheck \
	$(addprefix -I,$(BASE_INC_SEARCH_PATH) $(PROJ_INC_SEARCH_PATH) $(EXT_INC_SEARCH_PATH)) \
	--error-exitcode=1 $< 2> $@ 1> /dev/null
endef
