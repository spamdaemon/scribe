#include <scribe/RDBMS.h>
#include <scribe/sql/Row.h>
#include <scribe/sql/QueryResult.h>
#include <scribe/sql/SQLStatement.h>
#include <timber/config/Configuration.h>
#include <string>
#include <iostream>

using namespace ::timber;
using namespace ::scribe;
using namespace ::scribe::sql;
using namespace ::std;

using ::timber::config::Configuration;

static Reference<Connection> createConnection() throws (::std::exception)
{
  Reference<RDBMS> rdbms = DBMS::load("libpostgresql.so");

  auto cfg = Configuration::create();
  cfg->set("host","localhost");
  cfg->set("dbname","testdb");
  
  // load the database module
  return rdbms->connect(cfg);
}


void utest_test1()
{
  Pointer<Connection> con;
  try {
    con = createConnection();
  }
  catch (const ::std::exception& e) {
    ::std::cerr << "Could not connect to the database; that's ok because it's probably not running; " <<  e.what() << ::std::endl;
    return;
  }
  catch (...) {
    ::std::cerr << "Could not connect to the database; that's ok because it's probably not running" << ::std::endl;
    return;
  }
  Reference<SQLStatement> stmt = con->createSQLStatement();

  try {
    // just in case, drop the table from a previous test; we don't care if it fails
    stmt->setStatement("");
    stmt->stream() << " drop table A";
    stmt->execute();
  }
  catch (...) {
    // that's ok; it just means the table already existed from before
  }

  // here starts the real test
  stmt->setStatement(" create table A ( id integer, first_name varchar(200), last_name varchar(200) )");
  stmt->execute();
  
  stmt->setStatement("");
  stmt->stream() 
    << " SELECT * FROM A ";
  Reference<QueryResult> res = stmt->execute();
  ::std::cerr << "Number of rows retrieved " << res->rowCount() << ::std::endl;

  stmt->setStatement("INSERT INTO A VALUES (0, 'Winston','Churchill')");
  Reference<Result> ires = stmt->execute();
  ::std::cerr << "Number of rows inserted " << ires->rowCount() << ::std::endl;
  
  stmt->setStatement("");
  stmt->stream() 
    << " SELECT * FROM A ";
  res = stmt->execute();
  ::std::cerr << "Number of rows retrieved " << res->rowCount() << ::std::endl;
  
  ::std::cerr << "Column 1 " << (*res)->intValue(0) << ::std::endl;
  ::std::cerr << "Column 2 " << (*res)->stringValue(1) << ::std::endl;
  ::std::cerr << "Column 3 " << (*res)->stringValue(2) << ::std::endl;
  ::std::cerr << "Column 1 " << (*res)->intValue(res->columnIndex("id")) << ::std::endl;
  ::std::cerr << "Column 2 " << (*res)->stringValue(res->columnIndex("first_name")) << ::std::endl;
  ::std::cerr << "Column 3 " << (*res)->stringValue(res->columnIndex("last_name")) << ::std::endl;


  assert((*res)->intValue(0) == 0);
  assert((*res)->stringValue(1) == "Winston");
  assert((*res)->stringValue(2)=="Churchill");

  assert(res->columnIndex("id") == 0);
  assert(res->columnIndex("first_name") == 1);
  assert(res->columnIndex("last_name") == 2);


  stmt->setStatement("");
  stmt->stream() << " drop table A";
  stmt->execute();
}
