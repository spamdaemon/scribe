#include <postgresql/PostgresDBMS.h>
#include <scribe/sql/Connection.h>
#include <scribe/sql/Row.h>
#include <scribe/sql/RowType.h>
#include <scribe/sql/QueryResult.h>
#include <timber/calendar/GregorianCalendar.h>

#include <set>
#include <timber/logging.h>
#include <pgsql/libpq-fe.h>
#include <cstdio>

using namespace ::timber::logging;
using namespace ::scribe;
using namespace ::scribe::sql;

namespace postgresql {
  namespace {

    static inline bool verifyConnection (::PGconn* c) throws()
    { return c!=0 && ::PQstatus(c) == CONNECTION_OK; }

    static ::std::string escapeParameterValue (const ::std::string& str) throws()
    {
      ::std::string res;
      for (::std::string::size_type i=0;i<str.length();++i) {
	if (str[i]=='\"' || str[i]=='\\') {
	  res += '\\';
	}
	res += str[i];
      }
      return res;
    }

    struct PGBlob : public Blob {
      PGBlob (::canopy::UInt64 sz, char* data, bool mustFree) throws()
      : _size(sz),_data(data),_mustFree(mustFree) {}

      ~PGBlob() throws()
      {
	if (_mustFree) {
	  PQfreemem(_data);
	}
      }
	
      ::canopy::UInt64 size() const throws()
      { return _size; }

      char at (::canopy::UInt64 i) const throws(SQLException)
      {
	if (i<size()) {
	  return _data[i];
	}
	throw SQLException("Invalid index");
      }
    private:
      ::canopy::UInt64 _size;
      char* _data;
      const bool _mustFree;
    };
      
    struct PGConnection : public Connection {
      PGConnection (::timber::SharedRef< ::timber::config::Configuration> cfg);
      ~PGConnection () throws();
      ::timber::Reference<SQLStatement> createSQLStatement() throws (SQLException);
      void setDeferredConstraintsEnabled (bool deferred) throws(SQLException);
      ::timber::Reference<Schema> schema() throws(SQLException);
      bool isTransactionOpen() const throws();
      TransactionSupport getTransactionSupport() const throws();
      bool supportsAccessLevels () const throws();
      bool beginTransaction (IsolationLevel il, AccessLevel al) throws(SQLException);
      void commitTransaction() throws(SQLException);
      void rollbackTransaction() throws(SQLException);
      ::timber::Reference<SQLType> createType (SQLType::ID id) const throws(SQLException);
      bool supportsIsolationLevel (IsolationLevel level) const throws();
      ::timber::Reference<Result> executeSQL (const char* stmt) throws(SQLException);	

    private:
      PGconn* _connection;
    };

    struct PGStatement : public SQLStatement {
      PGStatement(const ::timber::Reference<PGConnection>& con) throws();
      ~PGStatement() throws();
      void setParameters(const Parameters& parms) throws(SQLException);
      void setParameter(const ::std::string& name,const ::std::string& value) throws(SQLException);
      void clearParameter(const ::std::string& name) throws(SQLException);
      ::timber::Reference<Result> execute () const throws(SQLException);
      void escapeBlob (const char* data, ::canopy::UInt64 n, ::std::string& res) const;

      void setStatement(const ::std::string& stmt) throws()
      {
	_query.str(stmt);
      }

      ::std::ostream& stream() throws ()
      { return _query; }

    private:
      Parameters _parameters;
      ::timber::Reference<PGConnection> _connection;
      ::std::ostringstream _query;
    };

    struct PGRow : public Row {
      PGRow (::PGresult* result) throws ();
      ~PGRow () throws ();

      ::canopy::UInt32 columnCount () const throws();
      bool isNull (ColumnIndex col) const throws(SQLException);
      ::std::string stringValue (ColumnIndex col) const throws(SQLException);
      ::canopy::Int64 longValue(ColumnIndex col) const throws(SQLException);
      ::canopy::Int32 intValue(ColumnIndex col) const throws(SQLException);
      ::canopy::Double doubleValue(ColumnIndex col) const throws(SQLException);
      ::canopy::Float floatValue(ColumnIndex col) const throws(SQLException);
      bool boolValue(ColumnIndex col) const throws(SQLException);
      ::timber::Date dateValue(ColumnIndex col) const throws(SQLException);
      ::timber::TimeOfDay timeValue(ColumnIndex col) const throws(SQLException);
      ::timber::Pointer<Blob> blobValue(ColumnIndex col) const throws(SQLException);

      void setRow (::canopy::Int64 r) throws();
      ::canopy::Int64 row () const throws();
	
    private:
      ::PGresult* _result;
      ::canopy::Int64 _row;
      ::canopy::UInt32 _columnCount;
    };

    struct PGResult : public Result {
      PGResult (const ::timber::Reference<PGConnection>& con, ::PGresult* res) throws(SQLException);
      ~PGResult () throws();
      ::canopy::UInt64 rowCount () const throws();

    private:
      ::timber::Reference<PGConnection> _connection;
      ::PGresult* _result;
      ::canopy::Int64 _rowCount;
    };

    struct PGQueryResult : public QueryResult {
      PGQueryResult (const ::timber::Reference<PGConnection>& con, ::PGresult* res) throws(SQLException);
      ~PGQueryResult () throws();
      ::canopy::UInt64 rowCount () const throws();
      ::canopy::UInt32 columnCount () const throws();
      ::canopy::UInt32 columnIndex (const ::std::string& name) const throws(SQLException);
      ::timber::Reference<SQLType> sqlType (RowType::ColumnIndex col) const throws(SQLException);
      ::std::string columnName (RowType::ColumnIndex col) const throws(SQLException);
      ::timber::Reference<RowType> info() const throws(SQLException);
	
      PGRow* operator->() throws(SQLException);	
      bool scroll() throws ();	
      bool scroll(::canopy::Int64 dr) throws ();
      bool scrollTo(::canopy::UInt64 r) throws ();

      ::canopy::UInt64 row() const throws (SQLException);

    private:
      ::timber::Reference<PGConnection> _connection;
      ::PGresult* _result;
      ::canopy::UInt32 _columnCount;
      ::canopy::Int64 _rowCount;
      ::timber::Pointer<RowType> _rowType;
      PGRow _row;
    };


    PGConnection::PGConnection (::timber::SharedRef< ::timber::config::Configuration> parms)
    {
      ::std::string connectionParms;
      
      const ::std::set< ::std::string> keys = parms->names();
      for (::std::set< ::std::string>::const_iterator i=keys.begin();i!=keys.end();++i) {
	auto cfgValue = parms->getValue(*i);
	if (!cfgValue) {
	  continue;
	}
	if (connectionParms.length()>0) {
	  connectionParms += ' ';
	}
	connectionParms += (*i);
	connectionParms += '=';
	connectionParms += '\'';
	connectionParms += escapeParameterValue(cfgValue->value());
	connectionParms += '\'';
      }
      
      _connection = PQconnectdb(connectionParms.c_str());
      if (_connection==0) {
	LogEntry("postgresql::PostgresDBMS",Level::WARN).stream() 
	  << "Could not connect to Postgres server" << ::std::endl
	  <<  "Parameters: " << connectionParms 
	  << commit;
	throw SQLException("Could not connect to Postgres server");
      }
	
      if (PQstatus(_connection) != CONNECTION_OK) {
	const ::std::string errmsg(::PQerrorMessage(_connection));
	LogEntry("postgresql::PostgresDBMS",Level::WARN).stream() 
	  << "Could not connect to Postgres server" << ::std::endl
	  << "Reson      : "+errmsg  << ::std::endl
	  <<  "Parameters: " << connectionParms
	  << commit;
	  
	::PQfinish(_connection);
	throw SQLException(::std::string("Could not connect to Postgres server. Reason: ")+errmsg);
      }
      LogEntry("postgresql::PostgresDBMS",Level::INFO).stream() 
	<< "Connected to Postgres server" << ::std::endl
	<<  "Parameters: " << connectionParms
	<< commit;
    }
      
    PGConnection::~PGConnection () throws()
    {
      ::PQfinish(_connection); 
    }
      
    ::timber::Reference<SQLStatement> PGConnection::createSQLStatement() throws (SQLException)
    {
      try {
	return new PGStatement(this); 
      }
      catch (const SQLException&) {
	throw;
      }
      catch (const ::std::exception& e) {
	throw SQLException(e.what());
      }
    }

    void PGConnection::setDeferredConstraintsEnabled (bool deferred) throws(SQLException)
    {
      throw SQLException("Not implemented");
    }
      
    ::timber::Reference<Schema> PGConnection::schema() throws(SQLException)
    {
      throw SQLException("Not implemented");
    }
      
    bool PGConnection::isTransactionOpen() const throws()
    {
      throw SQLException("Not implemented");
    }
      
    TransactionSupport PGConnection::getTransactionSupport() const throws()
    {
      throw SQLException("Not implemented");
    }
      
    bool PGConnection::supportsAccessLevels () const throws() 
    {
      throw SQLException("Not implemented");
    }
      
    bool PGConnection::beginTransaction (IsolationLevel il, AccessLevel al) throws(SQLException)
    {
      throw SQLException("Not implemented");
    }
      
    void PGConnection::commitTransaction() throws(SQLException) 
    {
      throw SQLException("Not implemented");
    }
      
    void PGConnection::rollbackTransaction() throws(SQLException) 
    {
      throw SQLException("Not implemented");
    }
      
    ::timber::Reference<SQLType> PGConnection::createType (SQLType::ID id) const throws(SQLException) 
    {
      throw SQLException("Not implemented");
    }
      
      
    bool PGConnection::supportsIsolationLevel (IsolationLevel level) const throws()
    { 
      return level == SERIALIZABLE 
	|| level== READ_COMMITTED; 
    }

      
    ::timber::Reference<Result> PGConnection::executeSQL (const char* stmt) throws(SQLException)
    {
      ::PGresult* res(0);
      try {
	if (verifyConnection(_connection)) {
	  res = ::PQexec(_connection,stmt);
	  switch ( ::PQresultStatus(res) ) {
	  case PGRES_TUPLES_OK:
	    return new PGQueryResult(this,res);
	  case PGRES_COMMAND_OK:
	    return new PGResult(this,res);
	  default:
	    break;
	  }
	}
	  
      }
      catch (const ::std::exception& e) {
	LogEntry("postgresql::PostgresDBMS",Level::WARN).stream() 
	  << "Exception caught while executing statement: " << stmt 
	  << ::std::endl << ": " << e.what() 
	  << commit;
	throw SQLException("Unknown exception during statment");
      }
      catch (...) {
	LogEntry("postgresql::PostgresDBMS",Level::WARN).stream() 
	  << "Unknown exception caught while executing statement: " << stmt 
	  << commit;
	throw SQLException("Unknown exception during statment");
      }
      const ::std::string errmsg(::PQresultErrorMessage(res));
      LogEntry("postgresql::PostgresDBMS",Level::WARN).stream() 
	<< "Could not execute statement" << ::std::endl
	<< "Reason    : " << errmsg << ::std::endl 
	<< "Statement : " << stmt 
	<< commit;
	
      throw SQLException(errmsg);
    }

      
    PGResult::PGResult (const ::timber::Reference<PGConnection>& con, ::PGresult* res) throws(SQLException)
    : _connection(con), _result(res),_rowCount(0)
    {
      switch (::PQresultStatus(res)) {
      case PGRES_COMMAND_OK:
	// an insert/update statement affected a certain number of tuples.
	if (::std::strlen(::PQcmdTuples(res)) > 0) {
	  unsigned long long cnt;
	  if (::std::sscanf(::PQcmdTuples(res) ,"%lld",&cnt)!=1) {
	    throw SQLException(::std::string("Could not obtain result size : ")+::PQcmdTuples(res));
	  }
	  _rowCount = cnt;
	}
	break;
      default:
	throw SQLException(::std::string("Postgres error: ")+ ::PQresultErrorMessage(res));
      }
    }

    PGResult::~PGResult () throws()
    {
      ::PQclear(_result);
      _result = 0;
    }

    ::canopy::UInt64 PGResult::rowCount () const throws()
    { return _rowCount; }
      
      
    PGQueryResult::PGQueryResult (const ::timber::Reference<PGConnection>& con, ::PGresult* res) throws(SQLException)
    : _connection(con), _result(res),_row(res)
    {
      switch (::PQresultStatus(res)) {
      case PGRES_TUPLES_OK: 
	// a query returned tuples
	_rowCount = ::PQntuples(res);
	_columnCount = ::PQnfields(res);
	break;
      default:
	throw SQLException(::std::string("Postgres error: ")+ ::PQresultErrorMessage(res));
      }
    }

    PGQueryResult::~PGQueryResult () throws()
    {
      ::PQclear(_result);
      _result = 0;
    }

    ::canopy::UInt64 PGQueryResult::rowCount () const throws()
    { return _rowCount; }

    ::canopy::UInt32 PGQueryResult::columnCount () const throws()
    { return _columnCount; }
      

    ::timber::Reference<RowType> PGQueryResult::info() const throws(SQLException)
    {
      throw SQLException("Unsupported operation");
    }

    ::canopy::UInt32 PGQueryResult::columnIndex (const ::std::string& name) const throws(SQLException)
    { 
      int i  = ::PQfnumber(_result,name.c_str());
      if (i<0) {
	throw SQLException(::std::string("Invalid column name : ")+name);
      }
      return i;
    }

    ::std::string PGQueryResult::columnName (::canopy::UInt32 col) const throws(SQLException)
    {
      if (col < _columnCount) {
	return ::PQfname(_result,col);
      }
      throw SQLException("Column out of bounds");
    }

    ::timber::Reference<SQLType> PGQueryResult::sqlType (RowType::ColumnIndex col) const throws(SQLException)
    {
      throw SQLException("Column out of bounds");
    }


      
    PGRow* PGQueryResult::operator->() throws(SQLException)
    {
      if (_columnCount==0) {
	throw SQLException("No rows available");
      }
      return &_row;
    }
      
    bool PGQueryResult::scroll() throws ()
    {
      ::canopy::Int64 r = 1+_row.row();
      if (r<_rowCount) {
	_row.setRow(r);
	return true;
      }
      return false;
    }
      
    bool PGQueryResult::scroll(::canopy::Int64 dr) throws ()
    {
      ::canopy::Int64 r = _row.row() + dr;
      if (r < 0 || r >= _rowCount) {
	return false;
      }
      _row.setRow(r);
      return true;
    }
      
    bool PGQueryResult::scrollTo(::canopy::UInt64 r) throws ()
    {
      ::canopy::Int64 xr(r);
      if (xr>=_rowCount) {
	return false;
      }
      _row.setRow(xr);
      return true;
    }

    ::canopy::UInt64 PGQueryResult::row() const throws (SQLException)
    {
      if (_rowCount==0) {
	throw SQLException("Empty result");
      }
      return _row.row(); 
    }


    PGStatement::PGStatement(const ::timber::Reference<PGConnection>& con) throws()
    : _connection(con) {}
      
    PGStatement::~PGStatement() throws()
    {}

    void  PGStatement::setParameters(const Parameters& parms) throws(SQLException)
    { _parameters = parms; }

    void  PGStatement::setParameter(const ::std::string& name,const ::std::string& value) throws(SQLException)
    { _parameters[name] = value; }

    void  PGStatement::clearParameter(const ::std::string& name) throws(SQLException)
    {
      Parameters::iterator i = _parameters.find(name);
      if (i!=_parameters.end()) {
	_parameters.erase(i);
      }
    }
      
    void PGStatement::escapeBlob (const char* data, ::canopy::UInt64 n, ::std::string& res) const
    {
      size_t len;
      unsigned char* escaped = ::PQescapeBytea(reinterpret_cast<const unsigned char*>(data),n,&len);
	
      res += reinterpret_cast<const char*>(escaped);
      ::PQfreemem(escaped);
    }


    ::timber::Reference<Result> PGStatement::execute () const throws(SQLException)
    { return _connection->executeSQL(_query.str().c_str()); }


    PGRow::PGRow(::PGresult* res) throws()
    : _result(res),_row(0),_columnCount(0) 
    {
      if (::PQresultStatus(res) == PGRES_TUPLES_OK) {
	// a query returned tuples
	_columnCount = ::PQnfields(res);
      }
    }

    PGRow::~PGRow() throws () 
    {}

    void PGRow::setRow (::canopy::Int64 r) throws()
    { _row = r; }

    ::canopy::Int64 PGRow::row () const throws()
    { return _row; }

    ::canopy::UInt32 PGRow::columnCount() const throws()
    { return _columnCount; }

    bool PGRow::isNull (ColumnIndex col) const throws(SQLException)
    { 
      if (col >= _columnCount) {
	throw SQLException("Invalid column");
      }
      return ::PQgetisnull(_result,_row,col);
    }

    ::std::string PGRow::stringValue (ColumnIndex col) const throws(SQLException)
    { 
      if (col >= _columnCount) {
	throw SQLException("Invalid column");
      }
      ::canopy::UInt64 len = ::PQgetlength(_result,_row,col);
      return ::std::string(::PQgetvalue(_result,_row,col),0,len);
    }

    ::canopy::Int64 PGRow::longValue(ColumnIndex col) const throws(SQLException)
    { 
      if (col >= _columnCount) {
	throw SQLException("Invalid column");
      }
      if (::PQgetisnull(_result,_row,col)!=0) {
	throw SQLException("Null column");
      }
      errno = 0;
      ::canopy::Int64 res = ::std::strtoll(::PQgetvalue(_result,_row,col),0,10);
      if (errno!=0) {
	throw SQLException("Invalid Int64");
      }
      return res;
    }
    ::canopy::Int32 PGRow::intValue(ColumnIndex col) const throws(SQLException)
    { 
      if (col >= _columnCount) {
	throw SQLException("Invalid column");
      }
      if (::PQgetisnull(_result,_row,col)!=0) {
	throw SQLException("Null column");
      }
      errno = 0;
      ::canopy::Int32 res = ::std::strtol(::PQgetvalue(_result,_row,col),0,10);
      if (errno!=0) {
	throw SQLException("Invalid Int32");
      }
      return res;
    }
    ::canopy::Double PGRow::doubleValue(ColumnIndex col) const throws(SQLException)
    { 
      if (col >= _columnCount) {
	throw SQLException("Invalid column");
      }
      if (::PQgetisnull(_result,_row,col)!=0) {
	throw SQLException("Null column");
      }
      errno = 0;
      ::canopy::Double res = ::std::strtod(::PQgetvalue(_result,_row,col),0);
      if (errno!=0) {
	throw SQLException("Invalid double");
      }
      return res;
    }
    ::canopy::Float PGRow::floatValue(ColumnIndex col) const throws(SQLException)
    { 
      if (col >= _columnCount) {
	throw SQLException("Invalid column");
      }
      if (::PQgetisnull(_result,_row,col)!=0) {
	throw SQLException("Null column");
      }
      errno = 0;
      ::canopy::Float res = ::std::strtof(::PQgetvalue(_result,_row,col),0);
      if (errno!=0) {
	throw SQLException("Invalid float");
      }
      return res;
    }
    bool PGRow::boolValue(ColumnIndex col) const throws(SQLException)
    { 
      if (col >= _columnCount) {
	throw SQLException("Invalid column");
      }
      if (::PQgetisnull(_result,_row,col)!=0) {
	throw SQLException("Null column");
      }
      if (::PQgetlength(_result,_row,col)==1) {
	char* t = ::PQgetvalue(_result,_row,col);
	if (*t=='f' || *t=='F') {
	  return false;
	}
	if (*t=='t' || *t=='T') {
	  return true;
	}
      }
      throw SQLException("Invalid bool");
    }
    ::timber::Date PGRow::dateValue(ColumnIndex col) const throws(SQLException)
    { 
      if (col >= _columnCount) {
	throw SQLException("Invalid column");
      }
      if (::PQgetisnull(_result,_row,col)!=0) {
	throw SQLException("Null column");
      }
      ::canopy::UInt64 len = ::PQgetlength(_result,_row,col);
      char* t = ::PQgetvalue(_result,_row,col);
      try {
	if (len>2 && t[0]=='\'') {
	  return ::timber::calendar::GregorianCalendar::parseISO8601(t+1,len-2);
	}
	else {
	  return ::timber::calendar::GregorianCalendar::parseISO8601(t,len);
	}
      }
      catch (...) {
	throw SQLException("Error parsing date");
      }
    }

    ::timber::TimeOfDay PGRow::timeValue(ColumnIndex col) const throws(SQLException)
    { 
      if (col >= _columnCount) {
	throw SQLException("Invalid column");
      }
      if (::PQgetisnull(_result,_row,col)!=0) {
	throw SQLException("Null column");
      }
      ::canopy::UInt64 len = ::PQgetlength(_result,_row,col);
      char* t = ::PQgetvalue(_result,_row,col);
      try {
	if (len>2 && t[0]=='\'') {
	  return ::timber::TimeOfDay::parseISO8601(t+1,len-2);
	}
	else {
	  return ::timber::TimeOfDay::parseISO8601(t,len);
	}
      }
      catch (...) {
	throw SQLException("Error parsing TimeOfDay");
      }
    }

    ::timber::Pointer<Blob> PGRow::blobValue(ColumnIndex col) const throws(SQLException)
    {
      if (col >= _columnCount) {
	throw SQLException("Invalid column");
      }
      if (::PQgetisnull(_result,_row,col)!=0) {
	throw SQLException("Null column");
      }
      size_t tolen;
      unsigned char* blob = PQunescapeBytea(reinterpret_cast<unsigned char*>(::PQgetvalue(_result,_row,col)),&tolen);
	
      return ::timber::Pointer<Blob> (new PGBlob(tolen,reinterpret_cast<char*>(blob),true));
    }
      
  }

  PostgresDBMS::PostgresDBMS() throws()
  {}
    
  PostgresDBMS::~PostgresDBMS() throws()
  {}
    
  ::timber::Reference<PostgresDBMS> PostgresDBMS::create() throws(::std::exception)
  {
    return new PostgresDBMS();
  }
    
  ::std::string PostgresDBMS::name() const throws()
  { return "postgresql"; }
    
  ::std::string PostgresDBMS::version() const throws()
  { return "7.4.0"; }
    
  ::timber::Reference<Connection> PostgresDBMS::connect (::timber::SharedRef< ::timber::config::Configuration> cfg) const throws(::std::exception)
  {
    return ::timber::Reference<Connection>(new PGConnection(cfg));
  }
}
