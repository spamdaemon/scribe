#ifndef _POSTGRESQL_POSTGRESDBMS_H
#define _POSTGRESQL_POSTGRESDBMS_H

#ifndef _SCRIBE_RDBMS_H
#include <scribe/RDBMS.h>
#endif

namespace postgresql {
  /**
   * This class is the implementation of the postgres database.
   * This is a singleton class.
   * @author Raimund Merkert
   * @date 30 Dec 2002
   * @todo Fix the singleton pattern implementation
   */
  class PostgresDBMS : public ::scribe::RDBMS {
    PostgresDBMS(const PostgresDBMS&);
    PostgresDBMS&operator=(const PostgresDBMS&);
      
    /**
     * Create a new PostgresDBMS object.
     */
  private:
    PostgresDBMS() throws();
	
    /**
     * Destroy this database
     */
  public:
    ~PostgresDBMS() throws();
	
    /**
     * Create a new instance of a Postgres System.
     * @return the PostgresDBMS
     */
  public:
    static ::timber::Reference<PostgresDBMS> create () throws (::std::exception);
	
    /**
     * Create an SQL connection to a database. The scheme
     * must be ignored when parsing the URI.
     * @param props the database properties
     * @return a new conncetion to a database (never 0)
     * @throw AccessException if the database cannot be accessed
     */
  public:
    ::timber::Reference< ::scribe::sql::Connection> connect (::timber::SharedRef< ::timber::config::Configuration> cfg) const throws (::std::exception);

    /**
     * Get the name of this DBMS.
     * @return the name of this dbms.
     */
  public:
    ::std::string name() const throws();
	
    /**
     * Get the name of this DBMS.
     * @return the name of this dbms.
     */
  public:
    ::std::string version() const throws();

  };
}

#endif
