#include <scribe/DBMSModule.h>
#include <timber/logging.h>
#include "PostgresDBMS.h"


extern "C" {
  
  namespace {
    static ::timber::Pointer< ::scribe::DBMS> createDB () throws()
    {
      try {
	return ::timber::Pointer< ::scribe::DBMS>(postgresql::PostgresDBMS::create());
      }
      catch (const ::std::exception& e) {
	::timber::logging::Log("Postgresql").caught("Could not create DBMS",e);
	return ::timber::Pointer< ::scribe::DBMS>();
      }
    }
  }

  extern DBMSModule dbmsModule = {
    createDB
  };
}
