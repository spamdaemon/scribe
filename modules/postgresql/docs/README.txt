To run the unit test for this module successfully the postgres server 
must be running:

This is what needs to be done:
1. initialize a dabase somewhere: 
     > ${POSTGRES_HOME}/bin/initdb

2. create a special database that is used by the unit test; it must be called
   testdb, otherwise the test must be modified to use a different database
     > ${POSTGRES_HOME}/bin/createdb testdb

3. run the postgres server:
     > ${POSTGRES_HOME}/bin/postmaster



